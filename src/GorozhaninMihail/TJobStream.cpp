﻿#include "tjobstream.h"
#include "TQueue.h"

void TJobStream::StartStream() 
{
	taskID = 0;
	int chance;
	for (long i = 0; i < tactsNum; i++)
		{
		chance = rand() % 100;
		if (chance < intense)
			{
			taskID++;
			if (CPunit.CanTakeTask())
				{
				CPunit.NewTask(taskID);
				}
			else
				deniels++; 
			}
		if (CPunit.IsBusy()) 
			{
			if (CPunit.WaitDone())
				cmpltd_tasks++;
			}
		else
			idle_tacts++; 
		}					 
	}


void TJobStream::PrintReport(int proc_perf, int queuesize, int tacts, int intens) 
{
	setlocale(LC_ALL, "rus");
	if (false)
		cout << "Ошибка" << endl;
	else
		{
		double temp;
		cout << "Количество заданий выданных процессору: " << taskID << endl;
		cout << "Размер очереди процессора: " << queuesize <<" заданий" << endl;
		cout << "Производительность процессора: " << proc_perf << " %" << endl;
		cout << "Общее время работы процессора: " << tacts <<" (в тактах)"<< endl;
		cout << "Общее время работы процессора без простоев: " << tacts-idle_tacts << " (в тактах)" << endl;
		cout << "Общее время простоя процессора: " << idle_tacts << " (в тактах)" << endl;
		cout << "Количество выполненных заданий: " << cmpltd_tasks << endl;
		cout << "Количество отказанных заданий: " << deniels << endl;
		cout << "Интенсивность потока заданий: " << intens << " %" << endl;
		temp = (deniels * 100.0) / taskID;
		cout << "Процент отказов: " << temp << " %" << endl;
		temp = (idle_tacts * 100.0) / tactsNum;
		cout << "Процент простоя: " << temp << " %" << endl;
		temp = (tactsNum - idle_tacts) / cmpltd_tasks;
		cout << "Среднее количество тактов на выполнение: " << temp << endl;
		cout << endl;

		}
	}