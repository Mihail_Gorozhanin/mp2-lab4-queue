#include <stdio.h>
#include "tdataroot.h"
TDataRoot::TDataRoot(int Size) : TDataCom(), MemSize(Size), DataCount(0) {
	if (Size == 0) 
		{
		pMem = NULL;
		MemType = MEM_RENTER;
		}
	else
		{
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
		}
	
}

TDataRoot :: ~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = NULL;
}

void TDataRoot::SetMem(void *p, int Size) 
{
	if (MemType == MEM_HOLDER)
		delete pMem; 
	MemType = MEM_RENTER;
	pMem = (TElem *)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty() const 
{
	return DataCount == 0;
}

bool TDataRoot::IsFull() const 
{
	return DataCount == MemSize;
}