#define SMPL_TEST
#ifdef SMPL_TEST

#include "TJobStream.h"

int main(int argc, char *argv[])
{
	
	int q1, q2, queuesize;
	long tacts;
	for (q1 = 10; q1 <= 40; q1 += 10)
	{
		q2 = 30;
		tacts = 10000000;
		queuesize = 8;
		TJobStream stream1(q2, queuesize, tacts, q1);
		stream1.StartStream();
		stream1.PrintReport(q2, queuesize, tacts, q1);
	}
	for (queuesize = 4; queuesize <= 16; queuesize += 4)
	{
		q1 = 20;
		q2 = 30;
		tacts = 10000000;
		TJobStream stream1(q2, queuesize, tacts, q1);
		stream1.StartStream();
		stream1.PrintReport(q2, queuesize, tacts, q1);
	}
	for (q2 = 10; q2 <= 40; q2 += 10)
	{
		q1 = 20;
		tacts = 10000000;
		queuesize = 8;
		TJobStream stream1(q2, queuesize, tacts, q1);
		stream1.StartStream();
		stream1.PrintReport(q2, queuesize, tacts, q1);
	}


	return 0;
}
#endif