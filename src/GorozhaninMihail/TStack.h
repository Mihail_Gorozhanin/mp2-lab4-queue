#ifndef _DATSTACK_H_
#define _DATSTACK_H_

#include "tdataroot.h"

class TStack : public TDataRoot
{
	protected:
		int Hi; 
		virtual int GetNextIndex(int Index);
	public:
		TStack(int Size = DefMemSize) : TDataRoot(Size)
		{
			Hi = -1;
		}
		virtual void Push(const TData &Val);
		virtual TData Pop(void); 
		virtual TData TopElem(void);
	protected:
		virtual int IsValid(void);	
		virtual void Print(void);	
};
#endif