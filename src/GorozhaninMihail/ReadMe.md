# Методы программирования 2: Очередь
***

### Введение:


Лабораторная работа направлена на практическое освоение динамической структуры данных **Очередь**.

С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. 

В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.

**Очередь (англ. queue)**, – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым.

###Требования к лабораторной работе

Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий:
 
- генерация нового задания;
- остановка задания в очередь для ожидания момента освобождения процессора;
- выборка задания из очереди при освобождении процессора после обслуживания очередного задания.

По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч.

- количество поступивших в ВС заданий;
- количество отказов в обслуживании заданий из-за переполнения очереди;
- среднее количество тактов выполнения заданий;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.

Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).


###Условия и ограничения
Сделаем следующие основные допущения: 
1 При планировании очередности обслуживания заданий возможность задания приоритетов не учитывается. 
2 Моменты появления новых заданий и моменты освобождения процессора рассматриваются как случайные события.

###Структуры данных
Напомним, что динамическая структура есть математическая структура, которой соответствует частично-упорядоченное (по включению) базовое множество **М**, операции вставки и удаления элементы которого являются структурами данных. При этом отношения включения индуцируются операциями преобразования структуры данных.

Таким образом, очередь есть динамическая структура, операции вставки и удаления переводят очередь из одного состояния в другое, при этом добавление новых элементов осуществляется в конец очереди, а извлечение – из начала очереди (дисциплина обслуживания «первым пришел – первым обслужен.

Важной задачей при реализации системы обслуживания очереди является выбор структуры хранения, обеспечивающей решение проблемы эффективного использования памяти без перепаковок и без использования связных списков (требующих дополнительных затрат памяти на указатели).

Как и в случае со стеком, в качестве структуры хранения очереди предлагается использовать одномерный (одноиндексный) массив, размещаемый в динамической области памяти. В связи с характером обработки значений, располагаемых в очереди, для указания хранимых в очереди данных необходимо иметь два указателя – на начало и конец очереди. Эти указатели увеличивают свое значение: один при вставке, другой при извлечении элемента. Таким образом, в ходе функционирования очереди может возникнуть ситуация, когда оба указателя достигнут своего наибольшего значения и дальнейшее пополнение очереди станет невозможным, несмотря на наличие свободного пространства в очереди. Одним из решений проблемы «движения» очереди является организация на одномерном массиве кольцевого буфера. Кольцевым буфером называется структура хранения, получаемая из вектора расширением отношения следования парой **p(an,a1)**.

####**Кольцевой буфер**
Структура хранения очереди в виде кольцевого буфера может быть определена как одномерный (одноиндексный) массив, размещаемый в динамической области памяти и расположение данных в котором определяется при помощи следующего набора параметров:

- pMem – указатель на память, выделенную для кольцевого буфера,
- MemSize – размер выделенной памяти,
- MaxMemSize – размер памяти, выделяемый по умолчанию, если при создании кольцевого буфера явно не указано требуемое количество элементов памяти,
- DataCount – количество запомненных в очереди значений,
- Hi – индекс элемента массива, в котором хранится последний элемент очереди,
- Li – индекс элемента массива, в котором хранится первый элемент очереди.

**Кольцевой буфер (на массиве)**

В связи с тем, что структура хранения очереди во многом аналогична структуре хранения стек, предлагается класс для реализации очереди построить наследованием от класса стек, описанного в лабораторной работе №3. При наследовании достаточно переопределить методы **Get** и **GetNextIndex**. В методе **Get** изменяется индекс для получения элемента (извлечение значений происходит из начала очереди), метод **GetNextIndex** реализует отношение следования на кольцевом буфере.

###Алгоритмы
Для работы с очередью предлагается реализовать следующие операции:

- Метод **Put** добавить элемент; Добавление элемента в очередь аналогично добавлению в стек, используется метод класса стек.
- Метод **Get** удалить элемент; При удалении элемента из очереди необходимо возвратить значение из динамического массива по индексу начала очереди, переместить указатель начала очереди и уменьшить количество элементов.
- Метод **IsEmpty** проверить очередь на пустоту;
- Метод **IsFull** проверить очередь на полноту.

Данные методы наследуются из класса **стек**.

###Моделирование
Для моделирования момента появления нового задания можно использовать значение датчика случайных чисел. Если значение датчика меньше некоторого порогового значения **q1, 0<=q1<=1**, то считается, что на данном такте имитации в вычислительную систему поступает новое задание (тем самым параметр **q1** можно интерпретировать как величину, регулирующую интенсивность потока заданий – новое задание генерируется в среднем один раз за **(1/q1)** тактов).

Моделирование момента завершения обработки очередного задания также может быть выполнено по описанной выше схеме. При помощи датчика случайных чисел формируется еще одно случайное значение, и если это значение меньше порогового значения **q2, 0<=q2<=1**,то принимается, что на данном такте имитации процессор завершил обслуживание очередного задания и готов приступить к обработке задания из очереди ожидания (тем самым параметр **q2** можно интерпретировать как величину, характеризующую производительность процессора вычислительной системы – каждое задание обслуживается в среднем за **(1/q2)** тактов ).

Возможная простая схема имитации процесса поступления и обслуживания заданий в вычислительной системе состоит в следующем.

- Каждое задание в системе представляется некоторым однозначным идентификатором (например, порядковым номером задания).
- Для проведения расчетов фиксируется (или указывается в диалоге) число тактов работы системы.
- На каждом такте опрашивается состояние потока задач и процессор.
- Регистрация нового задания в вычислительной системе может быть сведена к запоминанию идентификатора задания в очередь ожидания процессора. Ситуацию переполнения очереди заданий следует понимать как нехватку ресурсов вычислительной системы для ввода нового задания (отказ от обслуживания).
- Для моделирования процесса обработки заданий следует учитывать, что процессор может быть занят обслуживанием очередного задания, либо же может находиться в состоянии ожидания (простоя).
- В случае освобождения процессора предпринимается попытка его загрузки. Для этого извлекается задание из очереди ожидания.
- Простой процессора возникает в случае, когда при завершении обработки очередного задания очередь ожидающих заданий оказывается пустой.

После проведения всех тактов имитации производится вывод характеристик вычислительной системы:

- количество поступивших в вычислительную систему заданий в течение всего процесса имитации;
- количество отказов в обслуживании заданий из-за переполнения очереди – целесообразно считать процент отказов как отношение количества не поставленных в очередь заданий к общему количеству сгенерированных заданий, умноженное на 100%;
- среднее количество тактов выполнения задания;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания – целесообразно считать процент простоя процессора как отношение количества тактов простоя процессора к общему количеству тактов имитации, умноженное на 100%.

###Структура проекта

С учетом сформулированных выше предложений к реализации целесообразной представляется следующая модульная структура программы:
**TStack.h, TStack.cpp **– модуль с классом, реализующим операции над стеком;
**TQueue.h, TQueue.cpp** – модуль с классом, реализующим операции над очередью;
**TJobStream.h, TJobStream.cpp **– модуль с классом, реализующим поток задач;
**TProc.h, TProc.cpp** – модуль с классом, реализующим процессор;
**QueueTestkit.cpp** – модуль программы тестирования.

### Цели работы:   


##### В рамках лабораторной работы ставится задача разработки такой структуры данных как **очередь** основанной на структуре хранения стек (класс `TStack`);
 
С помощью разработанной **очереди** необходимо написать приложение, имитирующее однопроцессорную ЭВМ. 
***


### Задачи:  
1. Разработка класса всех необходимых классов перечисленных в пункте **Структура проекта**
2. Реализация методов всех необходимых классов согласно заданному интерфейсу.(см. пункт **Структура проекта**)
3. Разработка тестов для проверки работоспособности очереди.
4. Реализация алгоритма работы очереди.
5. Реализация вывода характеристик вычислительной системы в необходимом виде.
6. Обеспечение работоспособности тестов и примера использования.
***


### Используемые инструменты:


- Система контроля версий Git. 
- Фреймворк для написания автоматических тестов Google Test.
- Среда разработки Microsoft Visual Studio (2017).
***


# Начало работы:

***
### 1. Разработка и реализация класса `TDatacom`:
***

#### Объявление (h-файл):
```
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

class TDataCom
{
protected:
  int RetCode; 

  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```
***
### 2. Разработка и реализация класса `TDataroot`:
***
#### Объявление (h-файл):
```
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   10  
#define DataEmpty - 101  
#define DataFull - 102  
#define DataNoMem - 103  

typedef long    TElem;    
typedef TElem* PTElem;
typedef long    TData;   

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot : public TDataCom
{
protected:
	PTElem pMem;      
	int MemSize;      
	int DataCount;    
	TMemType MemType; 
	void SetMem(void *p, int Size);             
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;           
	virtual bool IsFull(void) const;          
	virtual void  Push(const TData &Val) = 0; 
	virtual TData Pop(void) = 0; 
	
	virtual int  IsValid(void) = 0;                 
	virtual void Print(void) = 0;                 
	
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

#endif
```
#### Реализация (cpp-файл):
```
#include <stdio.h>
#include "tdataroot.h"
TDataRoot::TDataRoot(int Size) : TDataCom(), MemSize(Size), DataCount(0) {
	if (Size == 0) 
		{
		pMem = NULL;
		MemType = MEM_RENTER;
		}
	else
		{
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
		}
	
}

TDataRoot :: ~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = NULL;
}

void TDataRoot::SetMem(void *p, int Size) 
{
	if (MemType == MEM_HOLDER)
		delete pMem; 
	MemType = MEM_RENTER;
	pMem = (TElem *)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty() const 
{
	return DataCount == 0;
}

bool TDataRoot::IsFull() const 
{
	return DataCount == MemSize;
}
```
***
### 3. Разработка и реализация класса `TStack`:
***
#### Объявление (h-файл):
```
#ifndef _DATSTACK_H_
#define _DATSTACK_H_

#include "tdataroot.h"

class TStack : public TDataRoot
{
	protected:
		int Hi; 
		virtual int GetNextIndex(int Index);
	public:
		TStack(int Size = DefMemSize) : TDataRoot(Size)
		{
			Hi = -1;
		}
		virtual void Push(const TData &Val);
		virtual TData Pop(void); 
		virtual TData TopElem(void);
	protected:
		virtual int IsValid(void);	
		virtual void Print(void);	
};
#endif
```
#### Реализация (cpp-файл):
```
#include <stdio.h>
#include "TStack.h"

void TStack::Push(const TData &Val) 
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsFull())
			SetRetCode(DataFull);
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}
}

TData TStack::Pop() 
{
	TData temp = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
	else
	{
		temp = pMem[Hi--];
		DataCount--;
	}
	return temp;
}

TData TStack::TopElem() 
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
	else
		return pMem[Hi];
}

int TStack::GetNextIndex(int index) { return ++index; } 

int  TStack::IsValid()
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}

void TStack::Print() 
{
	for (int i = 0; i < DataCount; i++)
		printf("%d", pMem[i]);
	printf("\n");
}
```

***
### 4. Разработка и реализация класса `TQueue`:
***
#### Объявление (h-файл):
```
#pragma once
#ifndef _TQUEUE_H_
#define _TQUEUE_H_

#include "TStack.h"

class TQueue : public TStack
{
protected:
	int Li; 
	virtual int GetNextIndex(int Index);
public:
	TQueue(int Size = DefMemSize) : TStack(Size)
	{
		Li = -1;
	}
	virtual void Put(const TData &Val); 
	virtual TData Get(void);
};
#endif
```
#### Реализация (cpp-файл):
```
#include <stdio.h>
#include "TQueue.h"

void TQueue::Put(const TData &Val) 
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsFull())
			SetRetCode(DataFull);
		else
		{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
		}
}


TData TQueue::Get(void) 
{
	TData temp = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
		Li = GetNextIndex(Li);
		temp = pMem[Li];
		DataCount--;
		}
	return temp;
}

int TQueue::GetNextIndex(int index) 
{
	if (index + 1 == MemSize)
		return 0;
	else
		return ++index;
}
```
***
### 5. Разработка и реализация класса `TProc`:
***
#### Объявление (h-файл):
```
#pragma once
#ifndef _TPROC_H_
#define _TPROC_H_

#include <ctime>
#include <stdlib.h>
#include "tqueue.h"

#define defpipe_size 10
#define defperf 50

class TProc
{
	private:
		int s = 10;
		TQueue pipeline;
		int perf; 
	public:
		TProc(int psize = defpipe_size, int pf = defperf) : pipeline(psize), perf(pf) { srand(time(0)); };
		bool CanTakeTask(); 
		bool IsBusy(); 
		bool NewTask(long task_id); 
		bool WaitDone(); 
};
#endif
```
#### Реализация (cpp-файл):
```
#include "TProc.h"

bool TProc::CanTakeTask() 
{
	if (!pipeline.IsFull())
		return true;
	else
		return false;
}

bool TProc::IsBusy() 
{
	if (!pipeline.IsEmpty())
		return true;
	else
		return false;
}

bool TProc::NewTask(long task_id) 
{
	if (CanTakeTask())
	{
		pipeline.Put(task_id);
		return true;
	}
	else
		return false;
}

bool TProc::WaitDone() 
{
	int chance = rand() % 100;
	if (chance < perf)
	{
		pipeline.Get();
		return true;
	}
	else
		return false;
}
```
***
### 6. Разработка и реализация класса `TJobStream`:
***
#### Объявление (h-файл):
```
#pragma once

#ifndef _TJOBSTRM_H_
#define _TJOBSTRM_H_

#include <iostream>
#include <locale>
#include "tproc.h"

using namespace std;

class TJobStream
{
	private:
		TProc CPunit;
		long tactsNum;
		long taskID; 
		long cmpltd_tasks = 0; 
		long idle_tacts = 0; 
		long deniels = 0; 
		int intense; 
		public:
			TJobStream(double proc_perf, int pipeline_len, long tactsNum, int intns) :
				CPunit(pipeline_len, proc_perf), tactsNum(tactsNum), intense(intns) {};
			void StartStream(); 
			void PrintReport(int proc_perf, int queuesize, int tacts, int intens);
};
#endif
```
#### Реализация (cpp-файл):
```
#include "tjobstream.h"
#include "TQueue.h"

void TJobStream::StartStream() 
{
	taskID = 0;
	int chance;
	for (long i = 0; i < tactsNum; i++)
		{
		chance = rand() % 100;
		if (chance < intense)
			{
			taskID++;
			if (CPunit.CanTakeTask())
				{
				CPunit.NewTask(taskID);
				}
			else
				deniels++; 
			}
		if (CPunit.IsBusy()) 
			{
			if (CPunit.WaitDone())
				cmpltd_tasks++;
			}
		else
			idle_tacts++; 
		}					 
	}


void TJobStream::PrintReport(int proc_perf, int queuesize, int tacts, int intens) 
{
	setlocale(LC_ALL, "rus");
	if (false)
		cout << "Ошибка" << endl;
	else
		{
		double temp;
		cout << "Количество заданий выданных процессору: " << taskID << endl;
		cout << "Размер очереди процессора: " << queuesize <<" заданий" << endl;
		cout << "Производительность процессора: " << proc_perf << " %" << endl;
		cout << "Общее время работы процессора: " << tacts <<" (в тактах)"<< endl;
		cout << "Общее время работы процессора без простоев: " << tacts-idle_tacts << " (в тактах)" << endl;
		cout << "Общее время простоя процессора: " << idle_tacts << " (в тактах)" << endl;
		cout << "Количество выполненных заданий: " << cmpltd_tasks << endl;
		cout << "Количество отказанных заданий: " << deniels << endl;
		cout << "Интенсивность потока заданий: " << intens << " %" << endl;
		temp = (deniels * 100.0) / taskID;
		cout << "Процент отказов: " << temp << " %" << endl;
		temp = (idle_tacts * 100.0) / tactsNum;
		cout << "Процент простоя: " << temp << " %" << endl;
		temp = (tactsNum - idle_tacts) / cmpltd_tasks;
		cout << "Среднее количество тактов на выполнение: " << temp << endl;
		cout << endl;
		}
	}
```

***
### 7. Разработка и реализация тестовой программы `QueueTestkit`:
***
#### Реализация (cpp-файл):
```
#define SMPL_TEST
#ifdef SMPL_TEST

#include "TJobStream.h"

int main(int argc, char *argv[])
{
	
	int q1, q2, queuesize;
	long tacts;
	for (q1 = 10; q1 <= 40; q1 += 10)
	{
		q2 = 30;
		tacts = 10000000;
		queuesize = 8;
		TJobStream stream1(q2, queuesize, tacts, q1);
		stream1.StartStream();
		stream1.PrintReport(q2, queuesize, tacts, q1);
	}
	for (queuesize = 4; queuesize <= 16; queuesize += 4)
	{
		q1 = 20;
		q2 = 30;
		tacts = 10000000;
		TJobStream stream1(q2, queuesize, tacts, q1);
		stream1.StartStream();
		stream1.PrintReport(q2, queuesize, tacts, q1);
	}
	for (q2 = 10; q2 <= 40; q2 += 10)
	{
		q1 = 20;
		tacts = 10000000;
		queuesize = 8;
		TJobStream stream1(q2, queuesize, tacts, q1);
		stream1.StartStream();
		stream1.PrintReport(q2, queuesize, tacts, q1);
	}


	return 0;
}
#endif
```
***
###8. Тесты для проверки **Queue** (GoogleTestFramework)
***
#####**Test_gtest.cpp**
```c++
#include <stdio.h>
#include <gtest/gtest.h>
#include "QueueGMU\TQueue.h"
#include "QueueGMU\tdataroot.h"
#include "QueueGMU\TStack.h"
#include "QueueGMU\TQueue.cpp"
#include "QueueGMU\tdataroot.cpp"
#include "QueueGMU\TStack.cpp"


TEST(TQueue, can_create_queue)
{
	ASSERT_NO_THROW(TQueue q1);
}

TEST(TQueue, can_create_queue_with_set_size)
{
ASSERT_NO_THROW(TQueue q1(5));
}

TEST(TQueue, cant_create_Queue_with_negative_length)      
{
	ASSERT_ANY_THROW(TQueue q1(-5));
}

TEST(TQueue, new_queue_is_empty)
{
TQueue q1;
EXPECT_EQ(true, q1.IsEmpty());
}

TEST(TQueue, can_put_elem_in_queue)
{
TQueue q1;
int elem = 1;
ASSERT_NO_THROW(q1.Put(elem));
}

TEST(TQueue, queue_with_elem_isnt_empty)
{
TQueue q1;
int elem = 1;
q1.Put(elem);
EXPECT_EQ(false, q1.IsEmpty());
}

TEST(TQueue, cant_put_in_full_queue)
{
TQueue q1(5);
int i = 1;
while (!q1.IsFull())
q1.Push(i++);
q1.Push(i + 1);
EXPECT_EQ(DataFull, q1.GetRetCode());
}


TEST(TQueue, can_get_elem_from_queue)
{
TQueue q1;
int elem1 = 1, elem2;
q1.Push(elem1);
elem2 = q1.Get();
EXPECT_EQ(elem1, elem2);
}

TEST(TQueue, cant_get_from_empty_queue)
{
TQueue q1;
q1.Get();
EXPECT_EQ(DataEmpty, q1.GetRetCode());
}

TEST(TQueue, get_returns_first_placed_elem)
{
TQueue q1;
int elem1 = 1, elem2 = 2, elem3 = 3, res;
q1.Put(elem1); q1.Put(elem2); q1.Put(elem3);
res = q1.Get();
EXPECT_TRUE(res == elem1);
}

TEST(TQueue, full_queue_after_get_isnt_full)
{
TQueue q1;
int i = 1;
while (!q1.IsFull())
q1.Push(i++);
bool isf = q1.IsFull();
int val = q1.Pop();
EXPECT_TRUE(isf != q1.IsFull());
}

TEST(TQueue, can_put_elem_in_full_queue_after_get)
{
TQueue q1;
int i = 1, temp;
while (!q1.IsFull())
q1.Put(i++);
temp = q1.Get() + 1;
q1.Put(temp);
while (!q1.IsEmpty())
i = q1.Get();
EXPECT_EQ(temp, i);
}

TEST(TQueue, can_put_any_value_into_queue)              
{
	TQueue q1(2);
	ASSERT_NO_THROW(q1.Put(5));
	ASSERT_NO_THROW(q1.Put(2));
	EXPECT_EQ(false, q1.IsEmpty());
}

TEST(TQueue, can_get_any_value_from_queue)               
{
	TQueue q1(2);
	q1.Put(5);
	q1.Put(2);
	EXPECT_EQ(5, q1.Get());
	EXPECT_EQ(2, q1.Get());
	EXPECT_EQ(1, q1.IsEmpty());
}

TEST(TQueue, size_test_of_queue)                   
{
	TQueue q1;
	for (int i = 0; i < 1000000; i++)
		ASSERT_NO_THROW(q1.Put(i));
}


TEST(TQueue, can_get_any_value_from_queue_2) 
{
	TQueue q1(1);
	bool Set_St = q1.IsEmpty(), Set_Fin;
	q1.Put(25);
	EXPECT_EQ(25, q1.Get());
	Set_Fin = q1.IsEmpty();
	EXPECT_EQ(Set_St, Set_Fin);

}

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

```
***
### 9. Обеспечение работоспособности тестов:
***

![](http://i013.radikal.ru/1704/57/e4403eeb70d2.png)
***
###10. Результат работы программы:
![](http://s019.radikal.ru/i640/1704/26/3e1f7f94e0c1.png)
![](http://s013.radikal.ru/i323/1704/2b/9c9e81588379.png)
![](http://s019.radikal.ru/i630/1704/a0/5e868d679115.png)
***
### 11.
***
**11.1 Полученные данные**
![](http://s019.radikal.ru/i617/1704/63/dac5e61982c9.png)
**11.2 **
Таким образом можно отследить определенные закономерности в 
различных блоках. 
####Блок 1
В данном блоке изменялась интенсивность потока заданий.
По данным таблицы отчетливо видно, что при увеличении интенсивности работа процессора улучшается касательно уменьшения всех характеристик зависящих от простоя процессора и негативно сказывается на отказах заданий.
####Блок 2
В данном блоке изменялась длина очереди заданий.
По данным таблицы также отчетливо видно, что при увеличении длины очереди работа процессора улучшается касательно уменьшения количества отказов и увеличения количества выполненных заданий процессором и в данном блоке показана наилучшая динамика роста по сравнению с другими блоками. Однако надо заметить, что статические показатели все равно являются более худшим результатом по сравнению с лучшими итерациями других блоков (например в итерации 4 блока 1 или итерации 1 блока 3).
####Блок 3
В данном блоке изменялась производительность процессора.
По данным таблицы также видно, что при увеличении производительности процессора работа процессора улучшается относительно количества выполненных заданий и уменьшении отказа в обслуживании, однако это приводит к простою процессора.

Таким образом, использую данные зависимости мы можем сымитировать любое возможное поведение процессора, с нужными нам характеристиками и желаемыми значениями выходных параметров.  

***
# Вывод:
***
##### Все цели и задачи работы достигнуты. В ходе выполнения данной работы, мне удалось разработать и реализовать несколько классов:
- **TStack.h, TStack.cpp **– модуль с классом, реализующим операции над стеком;
- **TQueue.h, TQueue.cpp** – модуль с классом, реализующим операции над очередью;
- **TJobStream.h, TJobStream.cpp **– модуль с классом, реализующим поток задач;
- **TProc.h, TProc.cpp** – модуль с классом, реализующим процессор;
- **QueueTestkit.cpp** – модуль программы тестирования. 
##### Также  мной были написаны тесты данному проекту под систему тестирования `Google Test`, что позволяет не только проверить правильность работы программы, но и также лучше ознакомиться с `Google Test Framework`. Успешное же прохождение всех тестов показывает, что классы были реализованы корректно и программа созданная с использованием этих классов работает верно. 
##### Особенностью данной работы было то, что:
1. Тесты для проверки корректности работы были написаны полностью самостоятельно, однако в связи с особенностью работы были сосредоточены на особенностях работы структуры **"Очередь"**
2. Была освоена структура **"Очередь"**
3. Реализация данной структуры была осуществлена на основе структуры **"Стек"** и класса `TStack` из предыдущей работы.
4. Также на основе данной структуры была создана имитация однопроцессорной ЭВМ. 
