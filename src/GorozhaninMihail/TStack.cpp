#include <stdio.h>
#include "TStack.h"

void TStack::Push(const TData &Val) 
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsFull())
			SetRetCode(DataFull);
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}
}

TData TStack::Pop() 
{
	TData temp = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
	else
	{
		temp = pMem[Hi--];
		DataCount--;
	}
	return temp;
}

TData TStack::TopElem() 
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
	else
		return pMem[Hi];
}

int TStack::GetNextIndex(int index) { return ++index; } 

int  TStack::IsValid()
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}

void TStack::Print() 
{
	for (int i = 0; i < DataCount; i++)
		printf("%d", pMem[i]);
	printf("\n");
}